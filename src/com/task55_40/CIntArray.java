package com.task55_40;

public class CIntArray implements ISumable {
	private int [ ] mIntArray;

	public int[] getmIntArray() {
		return mIntArray;
	}

	public void setmIntArray(int[] mIntArray) {
		this.mIntArray = mIntArray;
	}
	public void printValuemIntArray() {
		for(var i=0; i<this.mIntArray.length;i++) {
			System.out.println(this.mIntArray[i]);
		}
	}

	@Override
	public int getSum() {
		 int sum = 0;
		    for (int i = 0; i < this.mIntArray.length; i++) {
		        sum += this.mIntArray[i];
		    }
		    return sum;
	}
	
	public static void testSumable (ISumable paramISum) {
		System.out.println(paramISum.getSum());
	}
 
}
