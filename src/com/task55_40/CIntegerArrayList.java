package com.task55_40;

import java.util.ArrayList;

public class CIntegerArrayList implements ISumable {
	private ArrayList<Integer>  mIntegerArrayList;

	public ArrayList<Integer> getmIntegerArrayList() {
		return mIntegerArrayList;
	}

	public void setmIntegerArrayList(ArrayList<Integer> mIntegerArrayList) {
		this.mIntegerArrayList = mIntegerArrayList;
	}
	
	public void printmIntegerArrayList() {
		System.out.println(mIntegerArrayList);
	}


	@Override
	public int getSum() {
		 int sum = 0;
		    for (int i = 0; i < this.mIntegerArrayList.size(); i++) {
		        sum += this.mIntegerArrayList.get(i);
		    }
		    return sum;
	}
	
	public static void testSumable (ISumable paramISum) {
		System.out.println(paramISum.getSum());
	}
}
